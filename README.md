# database-client

## Requirements

For building and running the application you need:

- [JDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) at least
- [Maven 3](https://maven.apache.org)
- [PostgreSQL 12](https://www.postgresql.org/)

## Running the application locally

There are several ways to run a Spring Boot application on your local machine. One way is to execute the `main` method in the `com.valentyn.application.databaseclient.DatabaseClientApplication` class from your IDE.

Alternatively you can use the [Spring Boot Maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html) like below:

```shell
mvn spring-boot:run
```

## Project description
Spring boot application that work with PostgreSQL database.
Support features below:
- Saving, updating, listing and deleting connection details.
- Connected to chosen database.
- Listing schemas.
- Listing tables.
- Listing columns.
- Data preview of the table.
- Statistics about each column: min, max, avg, median value of the column.
- Statistics about each table: number of records, number of attributes.