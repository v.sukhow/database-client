package com.valentyn.application.databaseclient.handler;

import com.valentyn.application.databaseclient.exception.GlobalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class AppExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(AppExceptionHandler.class);

    @ExceptionHandler(GlobalException.class)
    public ResponseEntity<ApiResponse> globalHandler(GlobalException exception){
        logger.info(exception.getResponseCode() + ", " + exception.getMessage());
        return new ResponseEntity<>(new ApiResponse(exception.getResponseCode(), exception.getMessage()),
                exception.getResponseCode());
    }
}
