package com.valentyn.application.databaseclient.handler;

import org.springframework.http.HttpStatus;

public class ApiResponse {

    public HttpStatus status;

    public String message;

    public ApiResponse(HttpStatus status, String message) {
        this.status = status;
        this.message = message;
    }
}
