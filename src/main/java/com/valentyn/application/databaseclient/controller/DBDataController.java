package com.valentyn.application.databaseclient.controller;

import com.valentyn.application.databaseclient.dto.ColumnStatisticDto;
import com.valentyn.application.databaseclient.dto.ColumnsAnswerDto;
import com.valentyn.application.databaseclient.service.DBDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/db-data")
public class DBDataController {

    @Autowired
    private DBDataService dbDataService;

    @GetMapping("/listing-schemas/{database}")
    public ResponseEntity<List<String>> getSchemasByName(@PathVariable String database){

        List<String> schemas = dbDataService.getSchemas(database);
        return new ResponseEntity(schemas, HttpStatus.OK);
    }

    @GetMapping("/listing-tables/{database}")
    public ResponseEntity<List<String>> getTables(@PathVariable String database){

        List<String> tables = dbDataService.getTables(database);
        return new ResponseEntity(tables, HttpStatus.OK);
    }

    @GetMapping("/listing-columns/{database}/table/{table}")
    public ResponseEntity<ColumnsAnswerDto> getColumns(@PathVariable String database,
                                          @PathVariable String table){

        ColumnsAnswerDto columns = dbDataService.getColumns(database, table);
        return new ResponseEntity(columns, HttpStatus.OK);
    }

    @GetMapping("/listing-data/{database}/table/{table}")
    public ResponseEntity<List<Map<String, String>>> getData(@PathVariable String database,
                                                             @PathVariable String table){

        List<Map<String, String>> data = dbDataService.getData(database, table);
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    // Bonus tasks
    @GetMapping("/column-statistics/{database}/table/{table}")
    public ResponseEntity<List<ColumnStatisticDto>> getColumnsStatistic(@PathVariable String database,
                                                                        @PathVariable String table){
        List<ColumnStatisticDto> statistic = dbDataService.getColumnsStatistic(database, table);
        return new ResponseEntity<>(statistic, HttpStatus.OK);
    }

    @GetMapping("/table-statistic/{database}}")
    public ResponseEntity<List<Map<String, String>>> getTablesStatistic(@PathVariable String database) {

        List<Map<String, String>> tableStatistic = dbDataService.getTablesStatistic(database);
        return new ResponseEntity(tableStatistic, HttpStatus.OK);
    }
}
