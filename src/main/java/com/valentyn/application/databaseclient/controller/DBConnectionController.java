package com.valentyn.application.databaseclient.controller;

import com.valentyn.application.databaseclient.dto.PropsDto;
import com.valentyn.application.databaseclient.entity.Props;
import com.valentyn.application.databaseclient.service.DBConnectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/db-connection-postgres")
public class DBConnectionController {

    @Autowired
    DBConnectionService dbConnectionService;


    @GetMapping("/list")
    public ResponseEntity<List<PropsDto>> getAllDatabaseConnectionProperties(){
        List<PropsDto> propsDtoList = dbConnectionService.getAllDatabaseConnectionProperties();
        return new ResponseEntity<>(propsDtoList, HttpStatus.OK);
    }

    @GetMapping("/properties/{databaseId}")
    public ResponseEntity<PropsDto> getDatabaseConnectionProperties(@PathVariable UUID databaseId){
        PropsDto propsDto = dbConnectionService.getDatabaseConnectionProperties(databaseId);
        return new ResponseEntity<>(propsDto, HttpStatus.OK);
    }

    @DeleteMapping("/properties/{databaseId}")
    public ResponseEntity<Void> deleteDatabaseConnectionProperties(@PathVariable UUID databaseId){
        dbConnectionService.deleteDatabaseConnectionProperties(databaseId);
        return ResponseEntity.accepted().build();
    }

    @PostMapping
    public ResponseEntity<PropsDto> createDatabaseConnectionProperties(@RequestBody PropsDto propsDto){
        return new ResponseEntity(dbConnectionService.createDatabaseConnectionProperties(propsDto), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<PropsDto> updateDatabaseConnectionProperties(@RequestBody PropsDto propsDto,
                                                                       @PathVariable UUID id) throws SQLException {
        return ResponseEntity.ok(dbConnectionService.updateDatabaseConnectionProperties(propsDto, id));
    }

}
