package com.valentyn.application.databaseclient.config;

import com.valentyn.application.databaseclient.dbconnect.ConnectionWrapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    @Bean
    public ConnectionWrapper getConnections(){
        return new ConnectionWrapper();
    }
}
