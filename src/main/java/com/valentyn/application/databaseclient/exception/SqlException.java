package com.valentyn.application.databaseclient.exception;

import org.springframework.http.HttpStatus;

public class SqlException extends GlobalException {
    public SqlException(HttpStatus responseCode) {
        super(responseCode);
    }

    public SqlException(String message, HttpStatus responseCode) {
        super(message, responseCode);
    }

    public SqlException(String message, HttpStatus responseCode, String error) {
        super(message, responseCode, error);
    }
}
