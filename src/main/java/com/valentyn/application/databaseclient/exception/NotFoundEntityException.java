package com.valentyn.application.databaseclient.exception;

import org.springframework.http.HttpStatus;

import java.util.UUID;

public class NotFoundEntityException extends GlobalException {

    public NotFoundEntityException(int id) {
        super("entity not found com.valentyn.application.databaseclient.exception",
                HttpStatus.NOT_FOUND, "Cant fond entity by Id: " + id );
    }

    public NotFoundEntityException(UUID id) {
        super("entity not found com.valentyn.application.databaseclient.exception",
                HttpStatus.NOT_FOUND, "Cant fond entity by Id: " + id );
    }

    public NotFoundEntityException(HttpStatus responseCode) {
        super(responseCode);
    }

    public NotFoundEntityException(String message, HttpStatus responseCode) {
        super(message, responseCode);
    }

    public NotFoundEntityException(String message, HttpStatus responseCode, String error) {
        super(message, responseCode, error);
    }
}
