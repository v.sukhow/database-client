package com.valentyn.application.databaseclient.dbconnect.impl;

import com.valentyn.application.databaseclient.dbconnect.ConnectionWrapper;
import com.valentyn.application.databaseclient.dbconnect.DBConnection;
import com.valentyn.application.databaseclient.exception.SqlException;
import org.apache.tomcat.util.http.parser.HttpParser;
import org.postgresql.ds.PGSimpleDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.SQLException;

@Component
public class DBConnectionPostgres implements DBConnection {

    Logger logger = LoggerFactory.getLogger(DBConnectionPostgres.class);

    @Autowired
    private ConnectionWrapper connectionWrapper;

    @Override
    public void createConnection(String user, String password, String server, String database, int port) throws SQLException {

        PGSimpleDataSource dataSource = new PGSimpleDataSource();
        dataSource.setServerName(server);
        dataSource.setDatabaseName(database);
        dataSource.setUser(user);
        dataSource.setPassword(password);
        dataSource.setPortNumber(port);
        Connection connection = dataSource.getConnection();

        connectionWrapper.getConnectionMap().put(database, connection);
    }

    @Override
    public void closeConnection(Connection connection, String database) throws SQLException {

        connection.close();

        connectionWrapper.getConnectionMap().remove(database);
    }
}
