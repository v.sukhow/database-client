package com.valentyn.application.databaseclient.dbconnect;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public interface DBConnection {

    void createConnection(String user, String password, String server, String database, int port) throws SQLException;

    void closeConnection(Connection connection, String database) throws SQLException;
}
