package com.valentyn.application.databaseclient.dbconnect;

import java.sql.Connection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ConnectionWrapper {

    private Map<String, Connection> connectionMap = new ConcurrentHashMap<>();

    public Map<String, Connection> getConnectionMap() {
        return connectionMap;
    }
}
