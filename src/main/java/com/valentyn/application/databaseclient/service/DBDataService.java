package com.valentyn.application.databaseclient.service;

import com.valentyn.application.databaseclient.dto.ColumnStatisticDto;
import com.valentyn.application.databaseclient.dto.ColumnsAnswerDto;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public interface DBDataService {

    List<String> getTables(String database);

    ColumnsAnswerDto getColumns(String database, String tableName);

    List<String> getSchemas(String database);

    List<Map<String, String>> getData(String database, String tableName);

    List<ColumnStatisticDto> getColumnsStatistic(String database, String table);

    List<Map<String, String>> getTablesStatistic(String database);
}
