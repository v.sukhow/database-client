package com.valentyn.application.databaseclient.service.impl;

import com.valentyn.application.databaseclient.dbconnect.ConnectionWrapper;
import com.valentyn.application.databaseclient.dbconnect.DBConnection;
import com.valentyn.application.databaseclient.dto.PropsDto;
import com.valentyn.application.databaseclient.entity.Props;
import com.valentyn.application.databaseclient.exception.NotFoundEntityException;
import com.valentyn.application.databaseclient.exception.SqlException;
import com.valentyn.application.databaseclient.mapper.PropertyMapper;
import com.valentyn.application.databaseclient.repository.PropsRepository;
import com.valentyn.application.databaseclient.service.DBConnectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

@Service
public class DBConnectionServiceImpl implements DBConnectionService {

    @Autowired
    private PropsRepository propsRepository;

    @Autowired
    private PropertyMapper propertyMapper;

    @Autowired
    private ConnectionWrapper connectionWrapper;

    @Autowired
    private DBConnection dbConnection;


    @Override
    public List<PropsDto> getAllDatabaseConnectionProperties() {
        List<Props> propsList = propsRepository.findAll();
        return propertyMapper.propsListToPropsDto(propsList);
    }

    @Override
    public PropsDto getDatabaseConnectionProperties(UUID databaseId) {
        Props propById = propsRepository.findById(databaseId)
                .orElseThrow(() -> new NotFoundEntityException(databaseId));
        return propertyMapper.propsToPropsDto(propById);
    }

    @Override
    @Transactional
    public PropsDto createDatabaseConnectionProperties(PropsDto propsDto){
        createConnection(propsDto.username, propsDto.password,
                propsDto.hostname, propsDto.databaseName, propsDto.port);

        Props savedProps = propsRepository.save(propertyMapper.propsDtoToProps(propsDto));
        return propertyMapper.propsToPropsDto(savedProps);
    }

    @Override
    @Transactional
    public PropsDto updateDatabaseConnectionProperties(PropsDto propsDto, UUID id){
        if (!propsRepository.existsById(id)){
            throw  new NotFoundEntityException(id);
        }

        Props savedProps = propsRepository.save(propertyMapper.propsDtoToProps(propsDto, id));

        closeConnection(connectionWrapper.getConnectionMap().get(savedProps.databaseName), propsDto.databaseName);

        createConnection(propsDto.username, propsDto.password,
                propsDto.hostname, propsDto.databaseName, propsDto.port);
        return propertyMapper.propsToPropsDto(savedProps);
    }

    @Override
    @Transactional
    public void deleteDatabaseConnectionProperties(UUID databaseId){

        Props props = propsRepository.findById(databaseId)
                .orElseThrow(() -> new NotFoundEntityException(databaseId));
        Connection connection = connectionWrapper.getConnectionMap().get(props.databaseName);

        closeConnection(connection, props.databaseName);

        propsRepository.deleteById(databaseId);
    }

    private void createConnection(String user, String password, String server, String database, int port){
        try {
            dbConnection.createConnection(user, password,
                    server, database, port);
        } catch (SQLException e) {
            throw new SqlException("can not create connection", HttpStatus.CONFLICT);
        }

    }

    private void closeConnection(Connection connection, String database) {
        try {
            dbConnection.closeConnection(connection, database);
        } catch (SQLException e) {
            throw new SqlException("can not close connection", HttpStatus.CONFLICT);
        }
        connectionWrapper.getConnectionMap().remove(database);
    }

}
