package com.valentyn.application.databaseclient.service;

import com.valentyn.application.databaseclient.dto.PropsDto;
import com.valentyn.application.databaseclient.entity.Props;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

public interface DBConnectionService {

    List<PropsDto> getAllDatabaseConnectionProperties();

    PropsDto getDatabaseConnectionProperties(UUID databaseId);

    PropsDto createDatabaseConnectionProperties(PropsDto propsDto);

    PropsDto updateDatabaseConnectionProperties(PropsDto propsDto, UUID id);

    void deleteDatabaseConnectionProperties(UUID databaseId);
}
