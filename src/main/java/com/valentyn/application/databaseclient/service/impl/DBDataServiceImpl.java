package com.valentyn.application.databaseclient.service.impl;

import com.valentyn.application.databaseclient.dbconnect.ConnectionWrapper;
import com.valentyn.application.databaseclient.dto.*;
import com.valentyn.application.databaseclient.exception.SqlException;
import com.valentyn.application.databaseclient.service.DBDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DBDataServiceImpl implements DBDataService {

    Logger logger = LoggerFactory.getLogger(DBDataServiceImpl.class);

    @Autowired
    private ConnectionWrapper connectionWrapper;

    @Override
    public List<String> getTables(String database) {
        DatabaseMetaData metaData = getDatabaseMetaData(database);
        try {
            return getTables(metaData);
        } catch (SQLException e) {
            throw new SqlException(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @Override
    public ColumnsAnswerDto getColumns(String database, String tableName){

        DatabaseMetaData metaData = getDatabaseMetaData(database);

        try {
            return getColumns(metaData, database, tableName);
        } catch (SQLException e) {
            throw new SqlException(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @Override
    public List<String> getSchemas(String database) {

        DatabaseMetaData metaData = getDatabaseMetaData(database);
        try {
            return getSchemas(metaData);
        } catch (SQLException e) {
            throw new SqlException(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @Override
    public List<Map<String, String>> getData(String database, String tableName){
        try {
            return getDataResult(database, tableName);
        } catch (SQLException e) {
            throw new SqlException(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @Override
    public List<ColumnStatisticDto> getColumnsStatistic(String database, String tableName) {
        try {
            return getColumnsStatisticResult(database, tableName);
        } catch (SQLException e) {
            throw new SqlException(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @Override
    public  List<Map<String, String>> getTablesStatistic(String database) {
        try {
            return getTablesStatisticResult(database);
        } catch (SQLException e) {
            throw new SqlException(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    private DatabaseMetaData getDatabaseMetaData(String database){

        try {
            return getConnection(database).getMetaData();
        } catch (SQLException e) {
            logger.info(e.getMessage());
            throw new SqlException("can not get metadata", HttpStatus.CONFLICT);
        }

    }

    private Connection getConnection(String database){
        Connection connection = connectionWrapper.getConnectionMap().get(database);
        if (connection == null) {
            throw new SqlException("Connection not found", HttpStatus.NOT_FOUND);
        }
        return connection;
    }

    private ResultSet getResultSet(String database, String query) {

        Statement statement;
        ResultSet resultSet;
        try {
            statement = connectionWrapper.getConnectionMap().get(database).createStatement();
            resultSet = statement.executeQuery(query);
            resultSet.next();
        } catch (SQLException e) {

            logger.info(e.getMessage());
            return null;
        }

        return resultSet;
    }

    private List<String> getTables(DatabaseMetaData metaData) throws SQLException {
        List<String> tableList = new ArrayList<>();
        ResultSet tables = metaData.getTables(null, null, null, new String[]{"TABLE"});
        while (tables.next()) {
            String tableName = tables.getString("TABLE_NAME");
            tableList.add(tableName);
        }
        return tableList;
    }


    private ColumnsAnswerDto getColumns(DatabaseMetaData metaData, String database, String tableName) throws SQLException {

        ColumnsAnswerDto columnsAnswerDto = new ColumnsAnswerDto();
        ResultSet columns = metaData.getColumns(null, null, tableName, "%");
        while (columns.next()) {
            ColumnDto columnDto = new ColumnDto();
            columnDto.columnName = columns.getString("COLUMN_NAME");
            columnDto.columnSize = columns.getString("COLUMN_SIZE");
            columnDto.datatype = columns.getString("TYPE_NAME");
            columnDto.isNullable = columns.getString("IS_NULLABLE");
            columnDto.isAutoIncrement = columns.getString("IS_AUTOINCREMENT");
            columnsAnswerDto.columns.add(columnDto);
        }

        ResultSet primaryKeys = metaData.getPrimaryKeys(null, null, tableName);
        while (primaryKeys.next()) {
            PrimaryKeyDto primaryKeyDto = new PrimaryKeyDto();
            primaryKeyDto.primaryKeyColumnName = primaryKeys.getString("COLUMN_NAME");
            primaryKeyDto.primaryKeyName = primaryKeys.getString("PK_NAME");
            columnsAnswerDto.primaryKeys.add(primaryKeyDto);
        }

        ResultSet foreignKeys = metaData.getImportedKeys(null, null, tableName);
        while (foreignKeys.next()) {
            ForeignKeyDto foreignKeyDto = new ForeignKeyDto();
            foreignKeyDto.pkTableName = foreignKeys.getString("PKTABLE_NAME");
            foreignKeyDto.fkTableName = foreignKeys.getString("FKTABLE_NAME");
            foreignKeyDto.pkColumnName = foreignKeys.getString("PKCOLUMN_NAME");
            foreignKeyDto.fkColumnName = foreignKeys.getString("FKCOLUMN_NAME");
            columnsAnswerDto.foreignKeys.add(foreignKeyDto);
        }

        return columnsAnswerDto;
    }

    private List<String> getSchemas(DatabaseMetaData metaData) throws SQLException {

        List<String> schemaList = new ArrayList<>();
        ResultSet schemas = metaData.getSchemas();
        while (schemas.next()) {
            String columnName = schemas.getString("TABLE_SCHEM");
            schemaList.add(columnName);
        }
        return schemaList;
    }

    private List<Map<String, String>> getDataResult(String database, String tableName) throws SQLException {
        List<Map<String, String>> responseList = new ArrayList<>();

        ResultSet resultSetCount = getResultSet(database, "SELECT COUNT(*) FROM " + tableName);
        int rowCount = resultSetCount == null ? 0 : resultSetCount.getInt(1);

        ResultSet resultSet = getResultSet(database, "SELECT * FROM " + tableName);
        int columnCount = resultSet == null ? 0 : resultSet.getMetaData().getColumnCount();

        for (int i = 0; i < rowCount; i++) {
            resultSet.next();
            Map<String, String> response = new HashMap<>();
            for (int j = 1; j <= columnCount; j++) {
                response.put(resultSet.getMetaData().getColumnName(j), resultSet.getString(j));
            }
            responseList.add(response);
        }

        return responseList;
    }

    private List<ColumnStatisticDto> getColumnsStatisticResult(String database, String tableName) throws SQLException {

        ResultSet resultSet = getResultSet(database, "SELECT * FROM " + tableName);
        int columnCount = resultSet == null ? 0 : resultSet.getMetaData().getColumnCount();
        resultSet.next();

        List<String> columnNames = new ArrayList<>();
        for (int i = 1; i <= columnCount; i++) {
            columnNames.add(resultSet.getMetaData().getColumnName(i));
        }

        List<ColumnStatisticDto> columns = new ArrayList<>();
        for (int i = 0; i < columnCount; i++) {

            String columnName = columnNames.get(i);
            ColumnStatisticDto columnStatisticDto = new ColumnStatisticDto();
            columnStatisticDto.name = columnName;

            // get min
            resultSet = getResultSet(database, String.format("SELECT MIN(%s) FROM %s", columnName, tableName));
            columnStatisticDto.min = resultSet == null ? "" : resultSet.getString(1);

            //get max
            resultSet = getResultSet(database, String.format("SELECT MAX(%s) FROM %s ", columnName, tableName));
            columnStatisticDto.max = resultSet == null ? "" : resultSet.getString(1);

            //get avg
            resultSet = getResultSet(database, String.format("SELECT AVG(%s) FROM %s ", columnName, tableName));
            columnStatisticDto.avg = resultSet == null ? "" : resultSet.getString(1);

            //get median
            resultSet = getResultSet(database, String.format(" SELECT percentile_cont(0.5) within group" +
                    " ( order by %s ) FROM %s ", columnName, tableName));
            columnStatisticDto.median = resultSet == null ? "" : resultSet.getString(1);

            columns.add(columnStatisticDto);
        }

        return columns;
    }

    private  List<Map<String, String>> getTablesStatisticResult(String database) throws SQLException {
        List<String> tables = getTables(database);

        List<Map<String, String>> recordsCount = new ArrayList<>(tables.size());

        for (String table : tables) {
            ResultSet resultSetCount = getResultSet(database, "SELECT COUNT(*) FROM " + table);
            int rowCount = resultSetCount == null ? 0 : resultSetCount.getInt(1);

            Map<String, String> columnStatistic = new HashMap<>();
            columnStatistic.put("table name", table);
            columnStatistic.put("number of records", String.valueOf(rowCount));
            recordsCount.add(columnStatistic);


            //method getAttributes() does not implemented for postgres
            //exception :
            //  Method org.postgresql.jdbc.PgDatabaseMetaData.getAttributes(String,String,String,String) is not yet implemented.
//            DatabaseMetaData metaData = getDatabaseMetaData(database);
//            ResultSet attributes = metaData.getAttributes(table, null, null, null);
//            attributes.next();
        }
        return recordsCount;
    }
}
