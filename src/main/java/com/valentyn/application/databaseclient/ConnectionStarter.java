package com.valentyn.application.databaseclient;

import com.valentyn.application.databaseclient.dbconnect.DBConnection;
import com.valentyn.application.databaseclient.dto.PropsDto;
import com.valentyn.application.databaseclient.service.DBConnectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ConnectionStarter implements CommandLineRunner {

    @Autowired
    private DBConnectionService dbConnectionService;

    @Autowired
    private DBConnection dbConnection;

    @Override
    public void run(String... args) throws Exception {

        List<PropsDto> props = dbConnectionService.getAllDatabaseConnectionProperties();

        for (PropsDto propsDto : props){
            dbConnection.createConnection(propsDto.username, propsDto.password, propsDto.hostname,
                    propsDto.databaseName, propsDto.port);
        }

    }
}
