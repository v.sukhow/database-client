package com.valentyn.application.databaseclient.mapper;

import com.valentyn.application.databaseclient.dto.PropsDto;
import com.valentyn.application.databaseclient.entity.Props;
import org.mapstruct.Mapper;

import java.util.List;
import java.util.UUID;

@Mapper(componentModel = "spring")
public interface PropertyMapper {

    Props propsDtoToProps(PropsDto propsDto, UUID id);

    Props propsDtoToProps(PropsDto propsDto);

    PropsDto propsToPropsDto(Props props);

    List<Props> propsDtoListToProps(List<PropsDto> list);

    List<PropsDto> propsListToPropsDto(List<Props> list);

}
