package com.valentyn.application.databaseclient.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "property")
public class Props extends BaseEntity{

    @Column(name = "name")
    public String name;

    @Column(name = "host_name")
    public String hostname;

    @Column(name = "port")
    public Integer port;

    @Column(name = "database_name")
    public String databaseName;

    @Column(name = "user_name")
    public String username;

    @Column(name = "password")
    public String password;

}
