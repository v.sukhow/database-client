package com.valentyn.application.databaseclient.dto;

public class ForeignKeyDto {

    public String pkTableName;

    public String fkTableName;

    public String pkColumnName;

    public String fkColumnName;
}
