package com.valentyn.application.databaseclient.dto;

import java.util.ArrayList;
import java.util.List;

public class ColumnsAnswerDto {

    public List<ColumnDto> columns = new ArrayList<>();

    public List<PrimaryKeyDto> primaryKeys = new ArrayList<>();

    public List<ForeignKeyDto> foreignKeys = new ArrayList<>();
}
