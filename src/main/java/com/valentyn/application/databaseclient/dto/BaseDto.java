package com.valentyn.application.databaseclient.dto;

import java.time.LocalDateTime;
import java.util.UUID;

public class BaseDto {

    public UUID id;

    public LocalDateTime createdTime;

    public LocalDateTime updatedTime;

}
