package com.valentyn.application.databaseclient.dto;

public class PrimaryKeyDto {

    public String primaryKeyColumnName;

    public String primaryKeyName;

}
