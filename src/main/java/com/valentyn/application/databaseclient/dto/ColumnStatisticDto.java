package com.valentyn.application.databaseclient.dto;

public class ColumnStatisticDto {
    public String name;

    public String min;

    public String max;

    public String avg;

    public String median;
}
