package com.valentyn.application.databaseclient.dto;

public class PropsDto extends BaseDto{

    public String name;

    public String hostname;

    public Integer port;

    public String databaseName;

    public String username;

    public String password;

}
