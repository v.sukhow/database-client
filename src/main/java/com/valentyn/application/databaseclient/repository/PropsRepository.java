package com.valentyn.application.databaseclient.repository;

import com.valentyn.application.databaseclient.entity.Props;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface PropsRepository extends JpaRepository<Props, UUID> {
}
